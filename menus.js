const { Menu } = require("electron")


const template = [
    {
        label: "Edit",
        submenu: [
            { role: "undo" },
            { role: "redo" },
            { type: "separator" },
            { role: "cut" },
            { role: "copy" },
            { role: "paste" },
            { role: "pasteandmatchstyle" },
            { role: "delete" },
            { role: "selectall" }
        ]
    }
]

function setMenu() {
    if (process.platform === "darwin") {
        Menu.setApplicationMenu(Menu.buildFromTemplate(template))
    }
}

exports.setMenu = setMenu
