const http = require("http")
const path = require("path")

const { app, BrowserWindow, dialog, shell } = require("electron")
const finalhandler = require("finalhandler")
const serveStatic = require("serve-static")
const storage = require("./utils/promisified-json-storage")

const menus = require("./menus")


/**
 * The main window, containing the story.
 * @type {BrowserWindow}
 */
let mainWindow

/**
 * The loading window, displayed when the application starts.
 * @type {BrowserWindow}
 */
let loadingWindow

/**
 * The local server.
 * @type {http.Server}
 * */
let server

/**
 * Start the local server needed by Vorple, and then open the windows.
 *
 * The port is chosen automatically the first time the app is launched but is then saved and reused in subsequent play sessions, so that the local storage (holding Inform external files) stays the same between sessions.
 */
async function startServer() {
    try {
        var serverConfig = await storage.get("server-config")
    } catch(err) {
        throw err
    }

    var port = serverConfig.hasOwnProperty("port") ? serverConfig.port : 0
    var serve = serveStatic(path.join(__dirname, "/story"), { index: "play.html" })

    // Create the server.
    server = http.createServer((req, res) => {
        serve(req, res, finalhandler(req, res))
    })

    // Show an dialog and quit the app if the server encounters an error.
    server.on("error", (err) => {
        dialog.showErrorBox("Error while starting local server", `The local server could not be started. The application will now quit.\n\nError code: ${err.code}`)
        app.quit()
    })

    // Create the windows and store the port when starting listening.
    server.on("listening", async () => {
        if (port === 0) {
            try {
                await storage.set("server-config", { port: server.address().port })
            } catch(err) {
                throw err
            }
        }
        createWindows()
    })

    server.listen(port)
}

/**
 * Open the main window and the loading window.
 * Called after the server has started.
 *
 * For security, Node.js integration is disabled. A Vorple story will thus not be able to use the Node.js API, but will still be able to communicate with Electron thanks to the preload.js script. Disabling Node.js also has the benefit that we won't need to mess with JQuery in play.html.
 */
async function createWindows() {
    // Create the browser window.
    try {
        var metrics = await storage.get("main-window-metrics")
    } catch(err) {
        throw err
    }

    mainWindow = new BrowserWindow({
        width: metrics.hasOwnProperty("width") ? metrics.width : 800,
        height: metrics.hasOwnProperty("height") ? metrics.height : 600,
        x: metrics.hasOwnProperty("x") ? metrics.x : null,
        y: metrics.hasOwnProperty("y") ? metrics.y : null,
        fullscreen: metrics.hasOwnProperty("fullscreen") ? metrics.fullscreen : true,
        show: false,
        webPreferences: {
            nodeIntegration: false,
            preload: path.join(__dirname, "preload.js"),
            webviewTag: false
        }
    })
    global.mainWindow = mainWindow

    if (process.env.NODE_ENV !== "development") mainWindow.setMenu(null)

    // Load the play.html of the story.
    mainWindow.loadURL(`http://localhost:${server.address().port}/play.html`)

    // Maximize the window when it opens if it was maximized the last time.
    mainWindow.webContents.once("did-finish-load", () => {
        if (metrics.hasOwnProperty("maximized") && metrics.maximized) {
            mainWindow.maximize()
        }
    })

    // Make links open in the default browser.
    mainWindow.webContents.on("new-window", (event, url) => {
        event.preventDefault()
        shell.openExternal(url)
    })

    // Save the metrics of the window when it is resized or moved.
    var metricsTimeout

    mainWindow.on("resize", () => {
        clearTimeout(metricsTimeout)
        metricsTimeout = setTimeout(saveWindowMetrics, 500, mainWindow, "main-window-metrics")
    })

    mainWindow.on("move", () => {
        clearTimeout(metricsTimeout)
        metricsTimeout = setTimeout(saveWindowMetrics, 500, mainWindow, "main-window-metrics")
    })

    mainWindow.on("maximize", () => {
        clearTimeout(metricsTimeout)
        metricsTimeout = setTimeout(saveWindowMetrics, 500, mainWindow, "main-window-metrics")
    })

    mainWindow.on("unmaximize", () => {
        clearTimeout(metricsTimeout)
        metricsTimeout = setTimeout(saveWindowMetrics, 500, mainWindow, "main-window-metrics")
    })

    mainWindow.on("enter-full-screen", () => {
        clearTimeout(metricsTimeout)
        metricsTimeout = setTimeout(saveWindowMetrics, 500, mainWindow, "main-window-metrics")
    })

    mainWindow.on("leave-full-screen", () => {
        clearTimeout(metricsTimeout)
        metricsTimeout = setTimeout(saveWindowMetrics, 500, mainWindow, "main-window-metrics")
    })

    // When the window is closed.
    mainWindow.on("closed", () => {
        mainWindow = null
        global.mainWindow = null
    })

    // Create the loading window.
    loadingWindow = new BrowserWindow({
        width: 300,
        height: 170,
        resizable: false,
        movable: false,
        minimizable: false,
        maximizable: false,
        fullscreenable: false,
        show: false,
        frame: false,
        transparent: true,
        webPreferences: {
            nodeIntegration: false,
            webviewTag: false
        }
    })
    global.loadingWindow = loadingWindow

    loadingWindow.loadFile("./loading.html")

    loadingWindow.once("ready-to-show", () => {
        loadingWindow.show()
    })

    loadingWindow.on("closed", () => {
        loadingWindow = null
        global.loadingWindow = null
    })
}

/**
 * Get the metrics (size, position and so on) of a window.
 *
 * @param {BrowserWindow} win - The window of which we want the metrics.
 * @returns {{center: false, width: Number, height: Number, x: Number, y: Number, fullscreen: Number}} The metrics of the window.
 */
function getWindowMetrics(win) {
    if (!win) return

    var bounds = win.getNormalBounds()

    var metrics = {
        x: bounds.x,
        y: bounds.y,
        width: bounds.width,
        height: bounds.height,
        fullscreen: win.isFullScreen(),
        maximized: win.isMaximized()
    }

    return metrics
}

/**
 * Save the metrics of a window in the user storage.
 *
 * @param {BrowserWindow} win - The window of which we want to save the metrics.
 * @param {String} key - A key identifying the window.
 */
async function saveWindowMetrics(win, key) {
    if (!win) return

    try {
        await storage.set(key, getWindowMetrics(win))
    } catch(err) {
        throw err
    }
}

// Initialise the app when Electron is ready.
app.on("ready", () => {
    menus.setMenu()
    startServer()
})

// Quit the app when closing all windows.
app.on("window-all-closed", () => {
    app.quit()
})

// Shut down the server before quitting.
app.on("will-quit", (event) => {
    if (server && server.listening) {
        event.preventDefault()
        server.close((err) => {
            if (err) throw err
            app.quit()
        })
    }
})


// Require the rest of the app.
require("./listeners")
