const fs = require("fs")

const { app, dialog, ipcMain } = require("electron")

const customListeners = require("./custom-listeners")


var fileFilters = {
    glkdata: [
        { name: "Glk data file", extensions: ["glkdata"] }
    ],

    glksave: [
        { name: "Glk save file", extensions: ["glksave"] }
    ]
}

var listeners = {
    vorpleReady: () => {
        if (global.mainWindow) global.mainWindow.show()
        if (global.loadingWindow) global.loadingWindow.close()
        return null
    },

    getAppVersion: () => {
        return app.getVersion()
    },

    quit: () => {
        app.quit();
        return null
    },

    isFullScreen: () => {
        if (global.mainWindow) return global.mainWindow.isFullScreen()
        return false
    },

    toggleFullScreen: () => {
        if (global.mainWindow)
            global.mainWindow.setFullScreen(!global.mainWindow.isFullScreen())
        return null
    },

    setFullScreen: (flag) => {
        if (global.mainWindow) global.mainWindow.setFullScreen(flag)
        return null
    },

    saveExternalFile: (args) => {
        if (global.mainWindow) {
            var filename = dialog.showSaveDialog(global.mainWindow, {
                filters: fileFilters[args.type]
            })
            if (filename) {
                fs.writeFileSync(filename, args.data)
                return true
            } else {
                return false
            }
        }
        return false
    },

    loadExternalFile: (filters) => {
        if (global.mainWindow) {
            var filenames = dialog.showOpenDialog(global.mainWindow, {
                filters: fileFilters[filters],
                properties: ["openFile"]
            })
            if (filenames) {
                return fs.readFileSync(filenames[0], "binary")
            } else {
                return null
            }
        }
        return null
    }
}

Object.assign(listeners, customListeners)

ipcMain.on("vorplectron", (event, msg, arg) => {
    var returnValue = null

    if (listeners.hasOwnProperty(msg)) {
        returnValue = listeners[msg](arg)
    }

    event.returnValue = returnValue
})
