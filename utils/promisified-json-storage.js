const { promisify } = require("util")

const storage = require("electron-json-storage")


exports.get = promisify(storage.get)
exports.set = promisify(storage.set)
