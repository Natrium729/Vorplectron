/**
 * This script exposes ipcRenderer methods to the story window, so that Vorple can communicate with the Electron app.
 */

const { ipcRenderer } = require("electron");


const vorplectron = {
    send: (msg, arg) => {
        return ipcRenderer.sendSync("vorplectron", msg, arg)
    }
}

window.vorplectron = vorplectron
