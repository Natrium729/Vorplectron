# Vorplectron

**Warning: Vorplectron hasn't been updated for a long time, so don't use it as it is right now! I don't plan to work on it anymore, but if you need it and can't find a suitable alternative, get in touch and we'll see what we can do.**

Vorplectron is an Electron project that allow authors of interactive fictions to easily publish their [Vorple](https://vorple-if.com/) story as a standalone application.

# Quickstart

The following guide is aimed at people with no experience of JavaScript, Node.js or Electron. If that's not your case, juste skip the steps that are not relevant to you.

## Install Node

Go to the [Node.js download page](https://nodejs.org/en/download/), dowload the installer according to you operating system and install.

## Download this repository

Two ways:

* The easy one: go to the [Downloads section](https://bitbucket.org/Natrium729/vorplectron/downloads/), download the repository and extract the ZIP. If you do it that way, don't forget that you'll need to redo the modifications you make to this project every time you want you redownload Vorplectron (to update it, for example).
* The hard one: fork this repository and use your fork.

## Add your story files

You'll first have to download the Vorplectron Inform extension from [this repository](https://bitbucket.org/Natrium729/extensions-inform-7) and include it in your Inform project. This extension will allow your story to communicate with the Electron app (read it documentation for more information).

Add the following to your story:

```
Last Vorple interface setup:
	tell Vorplectron the story is ready.
```

Release your Inform story along with the Vorple interpreter and copy the files in the `<YourProject>.materials/Release` folder into the `story` folder of Vorplectron.

## Install the required Node.js modules

Open a command line in Vorplectron's folder, then type `npm install`. If Node.js was installed correctly (first step above), then everything Vorplectron needs will be installed.

If you're not sure how to open a command line in a specific folder, read the section concerning your operating system below.

### On Windows

Open Vorplectron's folder. Click on the path at the top of the window (just left of the search field). A field with the complete path will appear; delete the path, write `powershell` and press enter. You can now type `npm install` in the newly opened window, as written above.

### On macOS

You have to open the Terminal application. To find it easily, open Spotlight and type "terminal". Open the application with the `>_` icon.

In Terminal, type `cd` followed by a space then drag the Vorplectron folder onto the terminal window. Your command line should look like something like that: `$ cd /path/to/Vorplectron/folder`. Press enter. You can now type `npm install` as written above.

## Launch the app

When the installing has finished, type `npm start`. A window playing your story should appear.

Congratulations, you now have a Vorple story running as a standalone app! Next step: package it so you can distribute it to the world!

# Packaging the application

Coming soon!

# A small note on how it works.

All a Vorplectron app does is serve the `story` folder on a free port of localhost and load `http://localhost:<the chosen port>/play.html`. While it has been designed for a Vorple story, it does not have any Vorple-specific feature (for the moment at least). That means one could use Vorplectron as a template for any kind of simple JavaScript app if it fills one's needs.

# Licence

Vorplectron is built with [Electron](https://electronjs.org/) and released under the MIT licence. See `LICENSE.md` for more information.
